function validateInput() {

    var full_name = document.getElementById('full_name').value;
    var phone_number = document.getElementById('phone_number').value;
    var state = document.getElementById('select').value;
    var question = document.getElementById('question').value;
    var phone_helper_text = document.getElementById('phone_helper_text');

    var reg = /^\d{10}$|(\d{3}.\d{3}-\d{4}$)|\(\d{3}\)\ \d{3}\-\d{4}$/igm;

    var validPhone = phone_number.match(reg);

    if(!validPhone) {
        document.getElementById('phone_helper_text').className += ' invalidphonehelper';
        document.getElementById('phone_helper_text').innerHTML = ' * Phone Number - invalid phone number';
        document.getElementById('phone_number').className += ' invalidphoneinput';
    } else {
        document.getElementById('phone_helper_text').className = 'helper';
        document.getElementById('phone_helper_text').innerHTML = '* Phone Number:';
        document.getElementById('phone_number').className = 'input';
        document.getElementById('body').innerHTML = '<div class="helpText">Thank you!</div><br><p class="">Your information was submitted successfully. We will now connect you to next available chat agent.</p>';
    }
}